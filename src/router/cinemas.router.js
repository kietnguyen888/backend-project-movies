const { Router } = require("express");
const { getListCineplex, getListCinema } = require("../controler/cinemas.controller");

const cinemasRouter = Router();

cinemasRouter.get("/", getListCineplex);
cinemasRouter.get("/cinema-includes-cineplex",getListCinema)
module.exports = {
  cinemasRouter,
};
