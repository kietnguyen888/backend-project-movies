const { Router } = require("express");
const { authRouter } = require("./auth.router");
const { cinemasRouter } = require("./cinemas.router");
const { userRouter } = require("./user.router");
const rootRouter = Router();
//user đại dien cho get,post,put...
//  http://localhost:7000/api/v1/users
rootRouter.use("/users", userRouter);
//  http://localhost:7000/api/v1/auth
rootRouter.use("/auth",authRouter)
rootRouter.use("/cinemas",cinemasRouter)
//common js
module.exports = {
  rootRouter,
};
