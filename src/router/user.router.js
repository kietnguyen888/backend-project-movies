const { Router } = require("express");
const {
  findAllUser,
  findDetailUser,
  createUser,
  removeUser,
  updateUser,
  uploadAvatar,
} = require("../controler/user.controller");
const {
  authenticate,
  authorize,
} = require("../middleware/auth/verifyToken.middleware");
const {
  uploadImageSingle,
} = require("../middleware/upload/upload-image.middleware");
const {
  checkExist,
} = require("../middleware/validations/check-exist.middlewares");
const { User } = require("../models");
const userRouter = Router();
//tinh nang them xoa sua nen de phia ben tren voi get
userRouter.post(
  "/upload-avatar",
  authenticate,
  uploadImageSingle("avatar"),
  uploadAvatar
);
//  http://localhost:7000/api/v1/users

userRouter.get("/", findAllUser);
//  http://localhost:7000/api/v1/users/:id
userRouter.get("/:id", checkExist(User), findDetailUser);
userRouter.post("/", createUser);
userRouter.put("/:id", [checkExist(User)], updateUser);
userRouter.delete(
  "/:id",
  authenticate,
  authorize(["ADMIN","SUPER_ADMIN"]),
  checkExist(User),
  removeUser
);

//common js
module.exports = {
  userRouter,
};
