const multer = require("multer");
const { getFileExtension } = require("../../utils/getFileExtension");
const uploadImageSingle = (typeImage) => {
  const storage = multer.diskStorage({
    destination: (req, file, cb) => {
      cb(null, `./public/image/${typeImage}`);
    }, //duong dan thu muc de luu file
    filename: (req, file, cb) => {
      cb(null, Date.now() + "_" + file.originalname);
    }, // ten file goi len
  });

  const upload = multer({
    storage,
    fileFilter: (req, file, cb) => {
      const extensionImageList = ["png", "jpg", "jpeg", "gif", "webp"];
      const fileExtension = getFileExtension(file.originalname);
      if (extensionImageList.includes(fileExtension)) {
        cb(null, true);
      } else {
        cb(new Error("Extension ko hop le"));
      }
    },
  });
  return upload.single(typeImage);
};

// function cb(err, result) {
//   if (err) throw new Error(err);
//   return result;
// }

module.exports = {
  uploadImageSingle,
};
