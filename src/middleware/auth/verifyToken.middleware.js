const jwt = require("jsonwebtoken");

const authenticate = (req, res, next) => {
  const token = req.header("token");
  try {
    const secretKey = "kiet";
    const decode = jwt.verify(token, secretKey);
    console.log(decode);
    req.user = decode;
    next();
  } catch (err) {
    res.status(401).send({ message: "ban chua dang nhap" });
  }
};
const authorize = (arrayRole) => (req, res, next) => {
  const { user } = req;
  if (arrayRole.includes(user.role)) {
    next();
  } else {
    res.status(403).send("Ban đã đăng nhập , nhưng ko đủ quyền");
  }
};
module.exports = {
  authenticate,
  authorize,
};
