const getFileExtension = (fileName) => {
  const arrString = fileName.split(".");
  return arrString[arrString.length - 1];
};

module.exports = {
  getFileExtension,
};
