"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */

    await queryInterface.bulkInsert(
      "Movies",
      [
        {
          id: 1,
          name: "ironman 1",
          alias: "ironman 1",
          poster: "kink hinh",
          trailer: "lionk trailer",
          duration: "180",
          description: "phim nguoi sat",
          comingDate: "2021-11-19",
          createdAt: "2021-11-22 11:53:21",
          updatedAt: "2021-11-22 12:36:21",
        },
        {
          id: 2,
          name: "ironman 2",
          alias: "ironman 2",
          poster: "link hinh",
          trailer: "link trailer",
          duration: "180",
          description: "phim nguoi sat",
          comingDate: "2021-11-19",
          createdAt: "2021-11-22 11:53:21",
          updatedAt: "2021-11-22 12:36:21",
        },
      ],
      {}
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */

     await queryInterface.bulkDelete('Movies', null, {});
  },
};
