const { User } = require("../models");
const bcryptjs = require("bcryptjs");
const { userRouter } = require("../router/user.router");
const jwt = require("jsonwebtoken");
const signUp = async (req, res) => {
  try {
    const { name, email, password, phone } = req.body;
    /**
     * ma hoa password
     * 1/ tao chuoi ngau nhien
     * 2/ ket hop chuoi ngau nhien + password => hash
     */

    const salt = bcryptjs.genSaltSync(10);
    const hashPassword = bcryptjs.hashSync(password, salt);
    const newUser = await User.create({
      name,
      email,
      password: hashPassword,
      phone,
    });
    res.status(201).send(newUser);
  } catch (err) {
    res.status(500).send(err);
  }
};
const signIn = async (req, res) => {
  try {
    const { email, password } = req.body;
    const userLogin = await User.findOne({
      where: {
        email,
      },
    });
    if (userLogin) {
      const isAuth = bcryptjs.compareSync(password, userLogin.password);
      if (isAuth) {
        /**
         * tao user web token
         */
        const payload = {
          id: userLogin.id,
          email: userLogin.email,
          role: userLogin.role,
        };

        const secretKey = "kiet";
        const token = jwt.sign(payload, secretKey, { expiresIn: 24 * 60 * 60 });

        res.status(200).send({
          message: "Dang nhap thanh cong",
          token,
        });
      } else {
        res.status(404).send({ message: "Password ko chinh xac" });
      }
    } else {
      res.status(404).send({ message: "Email ko chinh xac" });
    }
  } catch (err) {
    res.status(500).send(err);
  }
};

const resetPassword = async (req, res) => {
  try {
    const { email } = req.body;
    const passwordDefault = "123";
    const userDetail = await User.findOne({
      where: {
        email,
      },
    });
    if (userDetail) {
      const salt = bcryptjs.genSaltSync(10);
      const hashPassword = bcryptjs.hashSync(passwordDefault, salt);
      userDetail.password = hashPassword;
      await userDetail.save();
      res.status(200).send({
        message: "reset password thanh cong",
        newPassword: passwordDefault,
      });
    } else {
      res.status(404).send("Email ko chinh xac");
    }
  } catch (err) {
    res.status(500).send(err);
  }
};
module.exports = {
  signIn,
  signUp,
  resetPassword,
};
