const { compareSync } = require("bcryptjs");
const { configEnv } = require("../config");
const { User, sequelize } = require("../models");
const bcryptjs = require("bcryptjs");

const userList = [
  {
    id: 1,
    name: "iron man",
    email: "iron@gmail.com",
    password: "123456",
    phone: "090909000",
    role: "ADMIN",
  },
  {
    id: 2,
    name: "spider man",
    email: "spider@gmail.com",
    password: "123456",
    phone: "090909000",
    role: "CLIENT",
  },
  {
    id: 5,
    name: "spider ",
    email: "spider@gmail.com",
    password: "123456",
    phone: "090909000",
    role: "CLIENT",
  },
];

const findAllUser = async (req, res) => {
  try {
    const userList = await User.findAll({
      // attributes: [
      //   "id",
      //   "name",
      //   "email",
      //   "phone",
      //   "role",
      //   "createdAt",
      //   "updatedAt",
      // ],
      attributes: {
        exclude: ["password"],
      },
    });
    res.status(200).send(userList);
  } catch (err) {
    res.status(500).send(err);
  }
};
const findDetailUser = async (req, res) => {
  try {
    const { id } = req.params;
    // const user = userList.find((item) => item.id === +id);
    // if (user) {
    //   res.status(200).send(user);
    // } else {
    //   res.status(404).send("ID ko ton tai");
    // }

    const detailUser = User.findByPk(id);
    res.status(200).send(deleteUser);
  } catch (err) {
    res.status(500).send(err);
  }
};

const createUser = async (req, res) => {
  try {
    const { name, email, password, phone, role } = req.body;
    // const newUser = { name, email, password, phone, role, id: Math.random() };
    // userList.push(newUser);
    // res.status(201).send(newUser);
    // c1
    // const newUser = User.build({ name, email, password, phone, role });
    // await newUser.save();
    // c2
    const salt = bcryptjs.genSaltSync(10);
    const hashPassword = bcryptjs.hashSync(password, salt);
    const newUser = await User.create({
      name,
      email,
      password: hashPassword,
      phone,
      role,
    });
    res.status(201).send(newUser);
  } catch (err) {
    res.status(500).send(err);
  }
};

// const deleteUser = (req, res) => {
//   try {
//     const { id } = req.params;
//     const userIndex = userList.findIndex((item, index) => item.id === +id);

//     if (userIndex === -1) {
//       res.status(404).send("ID ko ton tai");
//     } else {
//       userList.splice(userIndex, 1);
//       console.log(userList);
//       res.status(200).send("Delete Successfully");
//     }
//   } catch (err) {
//     res.status(500).send(err);
//   }
// };

// const updateUser = (req, res) => {
//   try {
//     const { id } = req.params;
//     const userIndex = userList.findIndex((item, index) => item.id === +id);
//     if (userIndex === -1) {
//       res.status(404).send("ID ko ton tai");
//     } else {
//       const { name, email, password, phone, role } = req.body;
//       userList[userIndex] = {
//         ...userList[userIndex],
//         name,
//         email,
//         password,
//         phone,
//         role,
//       };
//       res.status(200).send("Update Successfully");
//     }
//   } catch (err) {
//     res.status(500).send(err);
//   }
// };

const updateUser = async (req, res) => {
  try {
    const { id } = req.params;
    const { name, password, phone, role } = req.body;
    const result = await User.update(
      { name, password, phone, role },
      {
        where: {
          id,
        },
      }
    );
    const detailUser = await User.findByPk(id);
    res.status(200).send(detailUser);
  } catch (err) {
    res.status(500).send(err);
  }
};
const removeUser = async (req, res) => {
  try {
    const { id } = req.params;
    const detailUser = await User.findByPk(id);
    await User.destroy({
      where: {
        id,
      },
    });

    res.status(200).send(detailUser);
  } catch (err) {
    res.status(500).send(err);
  }
};
const uploadAvatar = async (req, res) => {
  const { user, file } = req;
  const urlImage = configEnv.server.host + file.path;
  //neu goi nhieu file thi se la files
  // const { user,files } = req
  const userUploadAvatar = await User.findByPk(user.id);
  userUploadAvatar.avatar = urlImage;
  await userUploadAvatar.save();
  res.status(200).send(userUploadAvatar);
};
module.exports = {
  findAllUser,
  createUser,
  findDetailUser,
  updateUser,
  removeUser,
  uploadAvatar,
};
