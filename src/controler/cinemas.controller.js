const { Cineplex, Cinema, sequelize } = require("../models");
const getListCineplex = async (req, res) => {
  try {
    const cineplexList = await Cineplex.findAll({
      include: [
        {
          model: Cinema,
        },
      ],
    });
    res.status(200).send(cineplexList);
  } catch (error) {
    res.status(500).send(error);
  }
};
const getListCinema = async (req, res) => {
  try {
    const cinemaList = await Cinema.findAll({
      include: [
        {
          model: Cineplex,
        },
      ],
    });
    const querySql = `select * from Cinemas`;
    const [results, metadata] = await sequelize.query(querySql);
    //trong thao tac get metadata ko quan trong, trong giao thuc PUT,DELELE,UPDATE dau khi thuc hien se thong bao  đã thao tac den bao nhieu row trong table
    res.status(200).send([results, metadata]);
  } catch (error) {
    res.status(500).send(error);
  }
};

module.exports = {
  getListCineplex,
  getListCinema,
};
