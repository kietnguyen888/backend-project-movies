const express = require("express");
const { rootRouter } = require("./src/router/root.router");
const app = express();
const dotenv = require("dotenv");
const path = require("path");
const { configEnv } = require("./src/config");
dotenv.config();
//setup dinh dang body thanh json
app.use(express.json());
//setup grapql


//setup static file
const pathPublicDirectory = path.join(__dirname, "./public"); //lay path tuyet doi toi thu muc hien tai
app.use("/public", express.static(pathPublicDirectory));
//http://localhost:7000 <=> folder public

app.get("/hello", (rep, res) => {
  res.send("xin chao nodejs 22");
});

//  http://localhost:7000/api/v1/users
app.use("/api/v1", rootRouter);

//http://localhost:7000
const port = process.env.PORT;
console.log({ port });

app.listen(port, () => {
  console.log("app run on port: " + port);
});
